<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' ]cw q-9@F[x|DPw.J}MlBFflJ*~gXuL*[%/pI3%UQ^ef$nT2nESy_#qXinuzno;' );
define( 'SECURE_AUTH_KEY',  'xugp+?wP]aAmZ`Ap|U*|){-yAsjMUuD~?6OK3lj61})hvgdW biRTW# 4NcYoE29' );
define( 'LOGGED_IN_KEY',    '8&5MiO<R6)d4XFgeFqo~(*J>WIN5i93lm2G(g^o]k^8!QNg<}a]pnUscj2,=B$Da' );
define( 'NONCE_KEY',        'Ri,E)&0-Z@ 5g&xLD!Wsm%rxEJI*6mRSMkZ-Up^ll~z[ylx+cxLR$$5lV}6fYk&B' );
define( 'AUTH_SALT',        '|1DV)+a7H:lQvahx!w,pD@*.*0MJ>t(L9k9*Smz+IkY}X~w>/Fo0`q@!wa 9PdL7' );
define( 'SECURE_AUTH_SALT', '^>|*7UBj?!ywDJP~u`Z%#v_EyZ)G5`D?Z/^:goA1wfQDzsM^u*x|KuW#_oMzZOw4' );
define( 'LOGGED_IN_SALT',   '+dk6ZH|}mPGznBBQ:W!heS6 [,Jowu,~u1FA|OHU~$K>v&=!-W6Hv2&25v.aY6Qw' );
define( 'NONCE_SALT',       'Xbm2l-f6Vh8[dt1Y`}q?f)4.uj4NqfIo*%5={~XpiNtG(+H4-{sY?7mII#s8*o)L' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
